package ru.mephi.labs.lab1;

import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class LinkedListTest {

    @Test
    void add() {
        var list = new LinkedList();
        for (int i = 0; i < 3; i++) {
            list.add(i);
        }
        list.add("qwe");
        var currentDate = new Date();
        list.add(currentDate);
        for (int i = 0; i < 3; i++) {
            assert list.get(i).equals(i);
        }
        assert list.get(3).equals("qwe");
        assert list.get(4).equals(currentDate);
    }

    @Test
    void addByIndex() {
        var list = new LinkedList();
        for (int i = 0; i < 3; i++) {
            list.add(i);
        }
        list.add(77, 2);
        assert list.get(2).equals(77);
    }

    @Test
    void remove() {
        var list = new LinkedList();
        for (int i = 0; i < 3; i++) {
            list.add(i);
        }
        list.add(99);
        assert list.remove(3).equals(99);
        assert list.size() == 3;
    }

    @Test
    void get() {
        var list = new LinkedList();
        for (int i = 0; i < 3; i++) {
            list.add(i);
        }
        for (int i = 0; i < 3; i++) {
            assert list.get(i).equals(i);
        }
    }

    @Test
    void set() {
        var list = new LinkedList();
        for (int i = 0; i < 3; i++) {
            list.add(i);
        }
        assert list.set(77, 2).equals(2);
        assert list.get(2).equals(77);
    }

    @Test
    void size() {
        var list = new LinkedList();
        for (int i = 0; i < 3; i++) {
            list.add(i);
        }
        assert list.size() == 3;
    }

    @Test
    void isEmpty() {
        var list = new LinkedList();
        for (int i = 0; i < 3; i++) {
            list.add(i);
        }
        assert !list.isEmpty();
    }

    @Test
    void contains() {
        var list = new LinkedList();
        for (int i = 0; i < 3; i++) {
            list.add(i);
        }
        list.add("qwe");
        assert list.contains("qwe");
    }

    @Test
    void indexOf() {
        var list = new LinkedList();
        for (int i = 0; i < 3; i++) {
            list.add(i);
        }
        list.add("qwe");
        list.add("asd");
        assert list.indexOf("asd") == 4;
    }
}