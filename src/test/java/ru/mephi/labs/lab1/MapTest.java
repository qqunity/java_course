package ru.mephi.labs.lab1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MapTest {

    @Test
    void put() {
        var dict = new Map();
        dict.put("key1", 123);
        var list = new LinkedList();
        for (int i = 0; i < 3; i++) {
            list.add(i);
        }
        dict.put("key2", list);
        assert dict.get("key1").equals(123);
        assert dict.get("key2").equals(list);
    }

    @Test
    void get() {
        var dict = new Map();
        for (int i = 0; i < 5; i++) {
            dict.put(i, i);
        }
        for (int i = 0; i < 5; i++) {
            assert dict.get(i).equals(i);
        }
    }

    @Test
    void getByDefault() {
        var dict = new Map();
        dict.put("key1", "value1");
        assert dict.get("key2", "default").equals("default");
        assert dict.size() == 2;
        assert dict.get("key2").equals("default");
    }

    @Test
    void remove() {
        var dict = new Map();
        dict.put("key1", "value1");
        dict.put("key2", "value2");
        assert dict.remove("key2").equals("value2");
        assert dict.size() == 1;
        assert dict.get("key2") == null;
    }
}