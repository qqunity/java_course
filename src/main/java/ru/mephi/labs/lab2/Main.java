package ru.mephi.labs.lab2;

import ru.mephi.labs.lab1.LinkedList;
import ru.mephi.labs.lab1.Map;

public class Main {
    public static void tokensCount(String s, char delimiter) {
        s  = s.replaceAll(delimiter + "+", String.valueOf(delimiter));
        var info = new Map();
        for (var token: s.split(String.valueOf(delimiter))) {
            if (info.keyContains(token)) {
                var curCount = (int) info.get(token);
                info.put(token, curCount + 1);
            } else {
                info.put(token, 1);
            }
        }
        var entries = info.getEntries();
        for (int i = 0; i < entries.size(); i++) {
            System.out.println(entries.get(i));
        }
    }

    public static void removeDuplicates(String s, char delimiter) {
        var tokens = new LinkedList();
        s  = s.replaceAll(delimiter + "+", String.valueOf(delimiter));
        var newS = "";
        for (var token: s.split(String.valueOf(delimiter))) {
            if (!tokens.contains(token)) {
                tokens.add(token);
                newS += token + delimiter;
            }
        }
        System.out.println(newS);
    }

    public static void main(String[] args) {
        var s1 = "aa    aaa    aa bb bbb     bbbb bb    ddd    uuu   iii ttt    ooo     aaa    ttt    ttt ttt    ttt";
        var s2 = "aa\t\taaa\t\t\t\taa\t\tbb\t\tbbb\t\tbbbb\tbb\t\t\tddd\t\tuuu\t\tttt\t\t\t\t\tiii\t\t\t\tooo\t\t\t\taaa\tttt\tttt\t\t\tttt\t\t\tttt";
        var s3 = "aa===aaa==aa===bb==bbb=bbbb=bb===ddd=uuu==iii=ttt==ooo===aaa=ttt===ttt=ttt===ttt";
//        tokensCount(s1, ' ');
//        tokensCount(s2, '\t');
        tokensCount(s3, '=');
//        removeDuplicates(s1, ' ');
//        removeDuplicates(s2, '\t');
//        removeDuplicates(s3, '=');
    }
}
