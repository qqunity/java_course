package ru.mephi.labs.lab3;

import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Locale;
import java.util.function.*;
import java.util.stream.Collectors;

public class Main {
    public static void streamTestWorks() {
        var testList = Employee.createShortList(11);
        var departments = new ArrayList<String>();

        System.out.println("Часть 1: ");
        System.out.print("Департаменты: ");
        testList.forEach(employee -> {
            departments.add(employee.getDept());
            System.out.print(employee.getDept() + "; ");
        });
        System.out.println();
        Account account = new Account(100000);

        System.out.println("Выплата премии женщинам сотрудникам:");
        testList.stream()
                .filter(employee -> employee.getGender() == Gender.FEMALE)
                .forEach(account::payPremium);

        System.out.println();

        System.out.println("Выплата зарплаты сотрудникам определенного департамента:");
        testList.stream()
                .filter(employee -> employee.getDept().equals(departments.get(0)))
                .forEach(account::paySalary);
        System.out.println();

        System.out.println("Выплата премии сотрудникам старше 30, работающим в определенном департаменте:");
        testList.stream()
                .filter(employee -> employee.getAge() > 30 &&
                        employee.getDept().equals(departments.get(1)))
                .forEach(account::payPremium);
        System.out.println();

        System.out.println("Выплата зарплаты менеджерам: ");
        testList.stream()
                .filter(employee -> employee.getRole() == Role.MANAGER)
                .forEach(account::paySalary);
        System.out.println();

        System.out.println("Выплата премии стаффу: ");
        testList.stream()
                .filter(employee -> employee.getRole() == Role.STAFF)
                .forEach(account::payPremium);
        System.out.println();


        System.out.println("Часть 2: ");

        System.out.println("Получить фамиилии и имена всех сотрудниц: ");
        var employees = testList.stream()
                .filter(employee -> employee.getGender().equals(Gender.FEMALE))
                .map(employee -> employee.getSurName() + " " + employee.getGivenName())
                .collect(Collectors.toList());
        employees.forEach(System.out::println);
        System.out.println();

        System.out.println("Отослать всем стафф сотрудникам сташе 18 приглашение на тренинг и добавить их почты в соответсвующий список:");
        Consumer<Employee> sendEMail = (employee) -> {
            System.out.println("Успешно отправлено письмо: " + employee.getEMail());
        };
        var mailingInfo = testList.stream().
                filter(employee -> employee.getRole().equals(Role.STAFF) && employee.getAge() > 18).
                peek(sendEMail).
                map(Employee::getEMail).
                collect(Collectors.toList());
        System.out.println(mailingInfo);
        System.out.println();

        System.out.println("Вывести всех сотрудников, у которых зарплата меньше средней:");
        var averageSalary = testList.stream().mapToDouble(Employee::getSalary).average().orElse(0.0);
        System.out.println("Средняя зарплата:" + averageSalary);
        testList.stream().filter(employee -> employee.getSalary() < averageSalary).forEach(System.out::println);
        System.out.println();

        System.out.println("Узнать есть ли парень из стафа, которому больше всего лет:");
        var maxAge = testList.stream()
                .mapToInt(Employee::getAge)
                .max().orElse(0);
        System.out.println("Максимальный возраст: " + maxAge);
        var result = testList.stream()
                .filter(employee -> employee.getRole().equals(Role.STAFF) && employee.getGender().equals(Gender.MALE))
                .anyMatch(employee -> employee.getAge() == maxAge);
        System.out.println(result);
        System.out.println();

        System.out.println("Получить девушку с наименьшей зарплатой");
        // можно было через min
        var empl = testList.stream()
                .filter(employee -> employee.getGender().equals(Gender.FEMALE))
                .sorted(Comparator.comparingDouble(Employee::getSalary))
                .findFirst().orElse(new Employee.Builder().build());
        System.out.println(empl);

    }

    public static void lambdaFunctionalTestWorks() {
        var testList = Employee.createShortList(11);

        System.out.println("Выводим имя и фамилию всех женщин сотрудников старше 40: ");
        Consumer<Employee> consumer = employee -> System.out.println(employee.getGivenName() + " " + employee.getSurName());
        Predicate<Employee> constraint = employee -> (employee.getDept().equals("Frontend-dept") ||
                employee.getDept().equals("QA-dept")) && employee.getAge() >= 1000;
        testList.stream().filter(constraint).forEach(consumer);
        System.out.println();

        System.out.println("Получаем электронные почты всех сотрудников для дальнейшей обработки: ");
        Function<Employee, String> function = Employee::getEMail;
        var eMails = testList.stream().map(function).collect(Collectors.toList());
        System.out.println(eMails);
        System.out.println();

        System.out.println("Создаем рандомного сотрудника женщину стаффа старше 30 лет:");
        var faker = new Faker(new Locale("en"));
        Supplier<Employee> supplier = () -> new Employee.Builder()
                .setGivenName(faker.name().firstName())
                .setSurName(faker.name().lastName())
                .setAge(faker.number().numberBetween(18, 65))
                .setGender(Gender.FEMALE)
                .setRole(Role.STAFF)
                .setDept(faker.job().title())
                .setEMail(faker.name().firstName().toLowerCase() + "@gmail.com")
                .setPhone(faker.phoneNumber().phoneNumber())
                .setAddress(faker.address().fullAddress())
                .setCity(faker.address().city())
                .setState(faker.address().state())
                .setCode(faker.address().countryCode())
                .setSalary(faker.random().nextInt(5000, 10000))
                .build();

        System.out.println(supplier.get());
        System.out.println("Поиск такого сотрудника в нашей тестовой базе: ");
        System.out.println(testList.stream().filter(employee -> employee.equals(supplier.get())).findFirst());
        System.out.println();

        System.out.println("Выбираем сотрудников с определенным набором почт: ");
        BiPredicate<Employee, String> biPredicate = ((employee, eMail) -> employee.getEMail().equals(eMail));
        var certainEmployees = new ArrayList<Employee>();
        for (var eMail :
                eMails.subList(0, 3)) {
            testList.stream().filter(employee ->
                    biPredicate.test(employee, eMail)).forEach(certainEmployees::add);
        }
        System.out.println(certainEmployees);
        System.out.println();
    }

    public static void main(String[] args) {
        streamTestWorks();
        lambdaFunctionalTestWorks();
    }
}
