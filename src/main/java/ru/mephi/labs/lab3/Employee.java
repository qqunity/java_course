package ru.mephi.labs.lab3;

import com.github.javafaker.Faker;

import java.util.*;

public class Employee {
    private String givenName;

    private String surName;

    private int age;

    private Gender gender;

    private Role role;

    private String dept;

    private String eMail;

    private String phone;

    private double salary;

    private String address;

    private String city;

    private String state;

    private String code;

    public Employee() {}

    public String getEMail() {
        return this.eMail;
    }

    public String getGivenName() {
        return this.givenName;
    }

    public Role getRole() {
        return this.role;
    }

    public String getSurName() {
        return this.surName;
    }

    public String getDept() {
        return this.dept;
    }

    public Gender getGender() {
        return this.gender;
    }

    public int getAge() {
        return this.age;
    }

    public double getSalary() {
        return this.salary;
    }

    public String getPhone() {
        return this.phone;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "givenName='" + givenName + '\'' +
                ", surName='" + surName + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                ", role=" + role +
                ", dept='" + dept + '\'' +
                ", eMail='" + eMail + '\'' +
                ", phone='" + phone + '\'' +
                ", salary=" + salary +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", code='" + code + '\'' +
                '}';
    }

    public static class Builder{

        private String givenName;

        private String surName;

        private int age;

        private Gender gender;

        private Role role;

        private String dept;

        private String eMail;

        private String phone;

        private String address;

        private String city;

        private String state;

        private double salary;

        private String code;

        public Builder setGivenName(String givenName) {
            this.givenName = givenName;
            return this;
        }

        public Builder setSurName(String surName) {
            this.surName = surName;
            return this;
        }

        public Builder setSalary(double salary) {
            this.salary = salary;
            return this;
        }

        public Builder setAge(int age) {
            this.age = age;
            return this;
        }

        public Builder setGender(Gender gender) {
            this.gender = gender;
            return this;
        }

        public Builder setRole(Role role) {
            this.role = role;
            return this;
        }

        public Builder setDept(String dept) {
            this.dept = dept;
            return this;
        }

        public Builder setEMail(String eMail) {
            this.eMail = eMail;
            return this;
        }

        public Builder setPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder setAddress(String address) {
            this.address = address;
            return this;
        }

        public Builder setCity(String city) {
            this.city = city;
            return this;
        }

        public Builder setState(String state) {
            this.state = state;
            return this;
        }

        public Builder setCode(String code) {
            this.code = code;
            return this;
        }

        public Employee build(){
            Employee employee = new Employee();
            employee.givenName = this.givenName;
            employee.surName = this.surName;
            employee.age = this.age;
            employee.gender = this.gender;
            employee.role = this.role;
            employee.dept = this.dept;
            employee.eMail = this.eMail;
            employee.phone = this.phone;
            employee.address = this.address;
            employee.city = this.city;
            employee.state = this.state;
            employee.code = this.code;
            employee.salary = this.salary;
            return employee;
        }
    }

    public static List<Employee> createShortList(int n){
        var roles = new Role[]{Role.STAFF, Role.MANAGER, Role.EXECUTIVE};
        var genders = new Gender[]{Gender.FEMALE, Gender.MALE};
        var faker = new Faker(new Locale("en"));
        var shortList = new ArrayList<Employee>();
        for (int i = 0; i < n; i++) {
            shortList.add(new Builder()
                    .setGivenName(faker.name().firstName())
                    .setSurName(faker.name().lastName())
                    .setAge(faker.number().numberBetween(18, 65))
                    .setGender(genders[faker.random().nextInt(2)])
                    .setRole(roles[faker.random().nextInt(3)])
                    .setDept(faker.job().title())
                    .setEMail(faker.name().firstName().toLowerCase() + "@gmail.com")
                    .setPhone(faker.phoneNumber().phoneNumber())
                    .setAddress(faker.address().fullAddress())
                    .setCity(faker.address().city())
                    .setState(faker.address().state())
                    .setCode(faker.address().countryCode())
                    .setSalary(faker.random().nextInt(5000, 10000))
                    .build());
        }
        return shortList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return age == employee.age && Double.compare(employee.salary, salary) == 0 && givenName.equals(employee.givenName) && surName.equals(employee.surName) && gender == employee.gender && role == employee.role && dept.equals(employee.dept) && eMail.equals(employee.eMail) && phone.equals(employee.phone) && address.equals(employee.address) && city.equals(employee.city) && state.equals(employee.state) && code.equals(employee.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(givenName, surName, age, gender, role, dept, eMail, phone, salary, address, city, state, code);
    }
}
