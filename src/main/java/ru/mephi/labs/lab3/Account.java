package ru.mephi.labs.lab3;

public class Account {
    private double balance;

    public Account(double initialBalance) {
        this.balance = initialBalance;
    }

    public double getBalance() {
        return balance;
    }

    public void addMoney(double amountOfMoney) {
        this.balance += amountOfMoney;
    }

    public void paySalary(Employee employee){
        this.balance -= employee.getSalary();
        System.out.println(employee +  " received salary " + "amount: " + employee.getSalary());
        this.checkCurrentBalance();
    }

    private void checkCurrentBalance() {
        if (this.balance < 0) {
            System.out.println("Balance less than 0!");
        }
        System.out.println("Balance: " + this.balance);
    }

    public void payPremium(Employee employee){
        var amountOfMoney = employee.getSalary() * employee.getRole().getBenefitPart();
        this.balance -= amountOfMoney;
        System.out.println(employee + " received " + employee.getRole().getBenefit() + " premium, " + "amount: " + amountOfMoney);
        this.checkCurrentBalance();
    }
}
