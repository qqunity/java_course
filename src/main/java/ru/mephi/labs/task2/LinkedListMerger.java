package ru.mephi.labs.task2;

import ru.mephi.labs.lab1.LinkedList;
import ru.mephi.labs.lab1.Node;

public class LinkedListMerger {

    private final LinkedList resultList;

    private void addToResultList(Node node1, Node node2) {
        if (node1 != null && node2 != null && node1.compareTo(node2) < 0) {
            this.resultList.add(node1.getValue());
            this.addToResultList(node1.getNext(), node2);
        } else if (node1 != null && node2 != null && node1.compareTo(node2) > 0) {
            this.resultList.add(node2.getValue());
            this.addToResultList(node1, node2.getNext());
        } else if (node1 == null && node2 != null) {
            this.resultList.add(node2.getValue());
            this.addToResultList(node1, node2.getNext());
        } else if (node2 == null && node1 != null) {
            this.resultList.add(node1.getValue());
            this.addToResultList(node1.getNext(), node2);
        }
    }

    public LinkedListMerger(Node head1, Node head2) {
        this.resultList = new LinkedList();
        this.addToResultList(head1, head2);
    }

    public Node getHead() {
        return this.resultList.getHead();
    }

    public void print() {
        this.resultList.print();;
    }
}
