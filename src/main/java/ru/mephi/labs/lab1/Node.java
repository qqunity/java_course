package ru.mephi.labs.lab1;

public class Node implements Cloneable, Comparable<Node> {
    private Node next = null;
    private Node prev = null;
    private Object value = null;
    private Boolean hasValue = false;

    public Node() {}



    public Node(Object value) {
        this.value = value;
        this.hasValue = true;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public void setPrev(Node prev) {
        this.prev = prev;
    }

    public Node getNext() {
        return this.next;
    }

    public Node getPrev() {
        return  this.prev;
    }

    public boolean checkValue() {
        return this.hasValue;
    }

    public void setValue(Object value) {
        this.value = value;
        this.hasValue = true;
    }

    public Object getValue()  {
        return this.value;
    }

    @Override
    protected Node clone() throws CloneNotSupportedException {
        return new Node(this.value);
    }

    @Override
    public int compareTo(Node node) {
        if (this.value.getClass() == Integer.class && node.getValue().getClass() == Integer.class) {
            if ((int) this.value < (int) node.getValue()) {
                return -1;
            } else {
                return 1;
            }
        }
        return 0;
    }
}
