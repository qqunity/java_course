package ru.mephi.labs.lab1;

import ru.mephi.labs.task2.LinkedListMerger;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        var l1 = new LinkedList();

        for (int i = 1; i < 5; i++) {
            l1.add(i);
        };
        l1.print();
        var l2 = new LinkedList();

        l2.add(3);
        l2.add(4);
        l2.add(7);
        l2.add(12);
        l2.add(14);
        l2.add(17);
        l2.print();

        var merged = new LinkedListMerger(l1.getHead(), l2.getHead());

        merged.print();



    }
}
