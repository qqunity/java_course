package ru.mephi.labs.lab1;

import java.util.Objects;

public class LinkedList implements Cloneable {
    private int length = 0;
    private Node head = null;
    private Node tail = null;

    public LinkedList() {}

    public Node getHead() {
        return this.head;
    }

    public Node getTail() {
        return this.tail;
    }

    public void add(Object value) {
        if (this.length == 0) {
            this.head = new Node(value);
            this.tail = head;
        } else {
            var element = new Node(value);
            this.tail.setNext(element);
            element.setPrev(tail);
            this.tail = element;
        }
        ++this.length;
    }

    public void add(Object value, int index) {
        if (index <= 0) {
            var element = new Node(value);
            this.head.setPrev(element);
            element.setNext(head);
            this.head = element;
            ++this.length;
        } else if (index >= this.length) {
            this.add(value);
        } else {
            var element = new Node(value);
            var buffElement = this.head;
            for (int i = 0; i < index - 1; i++) {
                buffElement = buffElement.getNext();
            }
            var nextBuffElement = buffElement.getNext();
            buffElement.setNext(element);
            element.setPrev(buffElement);
            element.setNext(nextBuffElement);
            nextBuffElement.setPrev(element);
            ++this.length;
        }
    }

    public Object remove(int index) {
        if (index < 0 || index >= this.length) {
            return null;
        } else if (index == 0) {
            var removedElement = this.head;
            this.head = this.head.getNext();
            this.head.setPrev(null);
            --this.length;
            return removedElement.getValue();
        } else if (index == this.length - 1){
            var removedElement = this.tail;
            this.tail = this.tail.getPrev();
            this.tail.setNext(null);
            --this.length;
            return removedElement.getValue();
        } else {
            var buffElement = this.head;
            for (int i = 0; i < index - 1; i++) {
                buffElement = buffElement.getNext();
            }
            var nextBuffElement = buffElement.getNext().getNext();
            var removedElement = buffElement.getNext();
            buffElement.setNext(nextBuffElement);
            nextBuffElement.setPrev(buffElement);
            --this.length;
            return removedElement.getValue();
        }
    }

    public Object get(int index) {
        if (index < 0 || index >= this.length) {
            return null;
        } else {
            var buffElement = this.head;
            for (int i = 0; i < index; i++) {
                buffElement = buffElement.getNext();
            }
            return buffElement.getValue();
        }
    }

    public Object set(Object value, int index) {
        if (index < 0 || index >= this.length) {
            return null;
        } else {
            var buffElement = this.head;
            for (int i = 0; i < index; i++) {
                buffElement = buffElement.getNext();
            }
            var oldElementValue = buffElement.getValue();
            buffElement.setValue(value);
            return oldElementValue;
        }
    }

    public int size() {
        return this.length;
    }

    public boolean isEmpty() {
        return this.length == 0;
    }

    public boolean contains(Object value) {
        var buffElement = this.head;
        for (int i = 0; i < this.length; i++) {
            if (buffElement.getValue().equals(value)) {
                return true;
            }
            buffElement = buffElement.getNext();
        }
        return false;
    }

    public int indexOf(Object value) {
        var buffElement = this.head;
        for (int i = 0; i < this.length; i++) {
            if (buffElement.getValue().equals(value)) {
                return i;
            }
            buffElement = buffElement.getNext();
        }
        return -1;
    }

    public void print() {
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        var buffElement = this.head;
        var out = "[";
        for (int i = 0; i < this.length; i++) {
            if (i == this.length - 1) {
                try {
                    out += buffElement.getValue() + "]";
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    out += buffElement.getValue() + ", ";
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            buffElement = buffElement.getNext();
        }
        return out;
    }

    @Override
    protected LinkedList clone() {
        var newLinkedList = new LinkedList();
        var buffElement = this.head;
        for (int i = 0; i < this.length; i++) {
            newLinkedList.add(buffElement.getValue());
            buffElement = buffElement.getNext();
        }
        return newLinkedList ;
    }

    public void resize(){
        this.add(null);
    }

    @Override
    public boolean equals(Object list) {
       if (list.getClass() != LinkedList.class) {
           return false;
       }
       if (((LinkedList) list).size() != this.size()) {
           return false;
       }
        for (int i = 0; i < this.size(); i++) {
            if (!((LinkedList) list).get(i).equals(this.get(i))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(length, head, tail);
    }
}
