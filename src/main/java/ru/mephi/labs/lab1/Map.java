package ru.mephi.labs.lab1;

import java.awt.*;

public class Map {
    private LinkedList table;
    private LinkedList deleted;
    private int size;

    public Map() {
        this.size = 0;
        this.table = new LinkedList();
        this.deleted = new LinkedList();
        for (int i = 0; i < 4; i++) {
            this.table.add(null);
            this.deleted.add(null);
        }
    }

    private static class KeyValueElement implements Cloneable {
        private final Object key;
        private Object value;

        KeyValueElement(Object key, Object value) {
            this.key = key;
            this.value = value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public Object getValue() {
            return this.value;
        }

        public Object getKey() {
            return this.key;
        }

        @Override
        public String toString() {
            return this.key + ": " + this.value;
        }

        @Override
        protected Object clone() throws CloneNotSupportedException {
            return super.clone();
        }
    }

    private int h1(Object element) {
        return element.hashCode() % this.table.size();
    }

    private int h2(Object element) {
        return 1 + element.hashCode() % (this.table.size() - 1);
    }

    public void put(Object key, Object value) {
        var element = new KeyValueElement(key, value);
        var isKeyContains = this.keyContains(key);
        var x = this.h1(element.getKey());
        var y = this.h2(element.getKey());
        for (int i = 0; i < this.table.size(); i++) {
            if ((this.table.get(x) == null || (Boolean) this.deleted.get(x)) && !isKeyContains) {
                this.table.set(element, x);
                this.deleted.set(false, x);
                ++this.size;
                return;
            } else if (this.table.get(x) != null && ((KeyValueElement) this.table.get(x)).getKey().equals(key) && isKeyContains) {
                this.table.set(element, x);
                this.deleted.set(false, x);
                return;
            }
            x = (x + y) % this.table.size();
        }
        this.table.resize();
        this.deleted.resize();
        this.put(key, value);
    }

    public Object get(Object key) {
        var x = this.h1(key);
        var y = this.h2(key);
        for (int i = 0; i < this.table.size(); i++) {
            if (this.table.get(x) != null) {
                if ((((KeyValueElement) this.table.get(x)).getKey().equals(key)) && !((Boolean) this.deleted.get(x))) {
                    return ((KeyValueElement) this.table.get(x)).getValue();
                }
            }
            x = (x + y) % this.table.size();
        }
        return null;
    }
    public Object get(Object key, Object byDefault) {
        var x = this.h1(key);
        var y = this.h2(key);
        for (int i = 0; i < this.table.size(); i++) {
            if (this.table.get(x) != null) {
                if ((((KeyValueElement) this.table.get(x)).getKey().equals(key)) && !((Boolean) this.deleted.get(x))) {
                    return ((KeyValueElement) this.table.get(x)).getValue();
                }
            }
            x = (x + y) % this.table.size();
        }
        this.put(key, byDefault);
        return byDefault;
    }

    public Object remove(Object key) {
        var x = this.h1(key);
        var y = this.h2(key);
        for (int i = 0; i < this.table.size(); i++) {
            if (this.table.get(x) != null) {
                if (((KeyValueElement) this.table.get(x)).getKey().equals(key)) {
                    var value = ((KeyValueElement) this.table.get(x)).getValue();
                    this.deleted.set(true, x);
                    this.table.set(null, x);
                    --this.size;
                    return value;
                }
            }
            x = (x + y) % this.table.size();
        }
        return null;
    }

    public boolean keyContains(Object key) {
        for (int i = 0; i < this.table.size(); i++) {
            if (this.table.get(i) != null && ((KeyValueElement) this.table.get(i)).getKey().equals(key)) {
                return true;
            }
        }
        return false;
    }

    public LinkedList getKeys() {
        var keys = new LinkedList();
        for (int i = 0; i < this.table.size(); i++) {
            if (this.table.get(i) != null) {
                keys.add(((KeyValueElement) this.table.get(i)).getKey());
            }
        }
        return keys;
    }

    public LinkedList getValues() {
        var values = new LinkedList();
        for (int i = 0; i < this.table.size(); i++) {
            if (this.table.get(i) != null) {
                values.add(((KeyValueElement) this.table.get(i)).getValue());
            }
        }
        return values;
    }

    public int size() {
        return this.size;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public LinkedList getEntries() {
        var entries = new LinkedList();
        for (int i = 0; i < this.table.size(); i++) {
            if (this.table.get(i) != null) {
                entries.add(((KeyValueElement) this.table.get(i)));
            }
        }
        return entries;
    }

    @Override
    public String toString() {
        var out = "{";
        var j = 0;
        for (int i = 0; i < this.table.size(); i++) {
            if (this.table.get(i) != null) {
                if (j != this.size - 1) {
                    out += ((KeyValueElement) this.table.get(i)).toString() + ", ";
                } else {
                    out += ((KeyValueElement) this.table.get(i)).toString();
                }
                ++j;
            }
        }
        out += "}";
        return out;
    }

    public void print() {
        System.out.println(this.toString());
    }
}
